package com.realdolmen.onstage.utils;

import com.realdolmen.onstage.beans.User;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

public class UserUtilTest {

    @Test
    public void testInvalidUsernameIsFalse(){
        String username = "----invalid$characters---";

        boolean result = UserUtil.verifyUsername(username);
        Assertions.assertFalse(result);
    }

    @Test
    public void testShortUsernameIsFalse(){
        String username = "sh";

        Assertions.assertFalse(UserUtil.verifyUsername(username));
    }

    @Test
    public void testLongUsernameIsFalse(){
        String username = "thisusernameisvalidbutitsactuallyveryveryverylong";

        Assertions.assertFalse(UserUtil.verifyUsername(username));
    }

    @Test
    public void usernameOnlyLettersIsTrue(){
        String username = "IAmValid";

        Assertions.assertTrue(UserUtil.verifyUsername(username));
    }

    @Test
    public void usernameOnlyNumbersIsTrue(){
        String username = "123456789";

        Assertions.assertTrue(UserUtil.verifyUsername(username));
    }

    @Test
    public void usernameMixedIsTrue(){
        String username = "MyNam3IsVal1d";

        Assertions.assertTrue(UserUtil.verifyUsername(username));
    }

    @Test
    public void emailWithoutAtIsFalse(){
        String email = "maximegmail.com";

        Assertions.assertFalse(UserUtil.verifyEmail(email));
    }

    @Test
    public void emailInvalidTLDIsFalse(){
        String email = "maxime@something";

        Assertions.assertFalse(UserUtil.verifyEmail(email));
    }

    @Test
    public void emailValidIsTrue(){
        String email = "maxime@gmail.com";

        Assertions.assertTrue(UserUtil.verifyEmail(email));
    }

    @Test
    public void weakPasswordIsFalse(){
        String password = "weak";

        Assertions.assertFalse(UserUtil.verifyPasswordStrength(password));
    }

    @Test
    public void strongPasswordIsTrue(){
        String password = "SomewHAtStrong9assw0rd";

        Assertions.assertTrue(UserUtil.verifyPasswordStrength(password));
    }
}
