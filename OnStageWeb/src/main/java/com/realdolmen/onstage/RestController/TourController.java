package com.realdolmen.onstage.RestController;

import com.fasterxml.jackson.annotation.JsonView;
import com.realdolmen.onstage.View.View;
import com.realdolmen.onstage.beans.Show;
import com.realdolmen.onstage.beans.Tour;
import com.realdolmen.onstage.dao.TourDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TourController {

    @Autowired
    private TourDao tourDao;

    @JsonView(View.Tour.class)
    @GetMapping("/api/tours/all")
    public List<Tour> all(
            @RequestParam(name = "page", defaultValue = "1") int page
    ) {
        return tourDao.findAll(page);
    }

    @JsonView(View.Tour.class)
    @GetMapping("/api/tours/id")
    public Tour byId(
            @RequestParam(name = "id", required = true) long id
    ) {
        return tourDao.findById(id);
    }

    @JsonView(View.Tour.class)
    @GetMapping("/api/tours/artist")
    public List<Tour> byArtist(
            @RequestParam(name = "artistId", required = true) long artistId,
            @RequestParam(name = "page", defaultValue = "1") int page
    ) {
        return tourDao.findByArtist(artistId, page);
    }

    @JsonView(View.Tour.class)
    @GetMapping("/api/tours/toppick")
    public List<Tour> byTopPick(
            @RequestParam(name = "topPicked", required = true) byte topPicked,
            @RequestParam(name = "page", defaultValue = "1") int page
    ) {
        boolean isPicked = (topPicked == 0) ? false : true;
        return tourDao.findByTopPick(isPicked, page);
    }

    @JsonView(View.Tour.class)
    @GetMapping("/api/tours/search")
    public List<Tour> byNameLike(
            @RequestParam(name = "search", required = true) String name,
            @RequestParam(name = "page", defaultValue = "1") int page
    ) {
        return tourDao.findByNameLike(name, page);
    }
}
