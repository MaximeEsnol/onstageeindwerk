package com.realdolmen.onstage.View;

public class View {
    /**
     * To be used to annotate fields such as
     * artist.id when returning a tour via the API.
     */
    public interface Tour{}

    public interface Show{}

    public interface Artist{}

    public interface User{}

    public interface FollowedArtist{}

    public interface PlannedShow{}

    public interface AuthKey{}
}
