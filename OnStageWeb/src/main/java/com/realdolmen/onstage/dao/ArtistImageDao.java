package com.realdolmen.onstage.dao;

import com.realdolmen.onstage.beans.ArtistImage;
import org.springframework.stereotype.Service;

import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.RollbackException;
import javax.persistence.TypedQuery;

@Service
public class ArtistImageDao extends Dao {
    public ArtistImage findById(long id){
        em = jpaUtil.createEntityManager();
        TypedQuery<ArtistImage> query = em.createNamedQuery("ArtistImage.findById", ArtistImage.class);
        query.setParameter("id", id);
        ArtistImage ai;
        try{
            ai = query.getSingleResult();
        } catch (NoResultException e){
            ai = null;
        }
        em.close();
        return ai;
    }

    public boolean add(ArtistImage ai){
        em = jpaUtil.createEntityManager();
        try{
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            em.persist(ai);
            tx.commit();
            return true;
        } catch (RollbackException e){
            return false;
        } finally {
            em.close();
        }
    }

    public boolean edit(ArtistImage ai){
        ArtistImage old = findById(ai.getId());
        em = jpaUtil.createEntityManager();
        try{
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            old.setPath(ai.getPath());
            old.setArtistId(ai.getArtistId());
            old.setUrl(ai.getUrl());
            tx.commit();
            return true;
        } catch (RollbackException e){
            return false;
        } finally {
            em.close();
        }
    }

    public boolean delete(int id){
        ArtistImage ai = findById(id);
        em = jpaUtil.createEntityManager();
        try{
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            if(!em.contains(ai)){
                ai = em.merge(ai);
            }
            em.remove(ai);
            tx.commit();
            return true;
        } catch (RollbackException e){
            return false;
        } finally {
            em.close();
        }
    }
}
