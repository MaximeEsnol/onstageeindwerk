package com.realdolmen.onstage.controller;

import com.realdolmen.onstage.beans.Artist;
import com.realdolmen.onstage.beans.ArtistImage;
import com.realdolmen.onstage.beans.ArtistSocial;
import com.realdolmen.onstage.beans.User;
import com.realdolmen.onstage.dao.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@Controller
public class DashboardController {

    @Autowired
    UserDao userDao;

    @Autowired
    ArtistDao artistDao;

    @Autowired
    ArtistSocialDao artistSocialDao;

    @Autowired
    ArtistImageDao artistImageDao;

    @GetMapping("/artist/dashboard")
    public ModelAndView artistDashboard(
            @RequestParam(name = "message", defaultValue = "") String message,
            ModelMap map,
            HttpSession httpSession
    ) {
        if(httpSession.getAttribute("user") != null){
            User user = (User) httpSession.getAttribute("user");
            switch(message){
                case "artistAdded":
                    map.addAttribute("message", "A new artist has been added to your account successfully.");
                    break;
                case "deleted":
                    map.addAttribute("message", "An artist has been deleted.");
                    break;
            }
            map.addAttribute("artists", artistDao.findByUser(user.getId(), 1));
            return new ModelAndView("/WEB-INF/dashboard/home.jsp");
        }
        return new ModelAndView("redirect:/signin");
    }

    @GetMapping("/artist/artist/new")
    public ModelAndView artistNew(
            @RequestParam(name = "error", defaultValue = "") String error,
            ModelMap map,
            HttpSession httpSession
    ) {
        if(httpSession.getAttribute("user") != null){
            User user = (User) httpSession.getAttribute("user");
            switch(error){
                case "failed":
                    map.addAttribute("error", "There was an error while creating a new artist.");
                    break;
            }
            return new ModelAndView("/WEB-INF/dashboard/artist/new.jsp");
        }
        return new ModelAndView("redirect:/signin");
    }

    @GetMapping("/artist/{id}")
    public ModelAndView artistDetails(
            @PathVariable(name = "id") int id,
            ModelMap map,
            HttpSession httpSession
    ) {
        if(httpSession.getAttribute("user") != null){
            User user = (User) httpSession.getAttribute("user");
            Artist artist = artistDao.byIdFromUser(id, user.getId());

            if(artist != null){
                map.addAttribute("artist", artist);
            } else {
                map.addAttribute("error", "noaccess");
            }

            return new ModelAndView("/WEB-INF/dashboard/artist/details.jsp");
        }
        return new ModelAndView("redirect:/signin");
    }

    @GetMapping("/artist/{id}/delete")
    public ModelAndView deleteArtist(
            @PathVariable(name = "id") int id,
            HttpSession httpSession
    ) {
        if(httpSession.getAttribute("user") != null){
            User user = (User) httpSession.getAttribute("user");
            Artist artist = artistDao.byIdFromUser(id, user.getId());
            if(artist != null){
                if(artistDao.delete(id)){
                    return new ModelAndView("redirect:/artist/dashboard?message=deleted");
                }
            } else {
                return new ModelAndView("redirect:/artist/" + id);
            }
        }

        return new ModelAndView("redirect:/artist/" + id);
    }

    @PostMapping("/artist/{id}")
    public ModelAndView editArtist(
            @PathVariable(name = "id") int id,
            @RequestParam Map<String, String> params,
            HttpSession httpSession
    ) {
        if(httpSession.getAttribute("user") != null){
            User user = (User) httpSession.getAttribute("user");
            Artist artist = artistDao.byIdFromUser(id, user.getId());
            if(artist != null){
                Artist generatedArtist = handleArtistCreation(params, httpSession);
                generatedArtist.setId(id);
                if(artistDao.edit(generatedArtist)){
                    return new ModelAndView("redirect:/artist/"+id+"?message=artistEdited");
                } else {
                    return new ModelAndView("redirect:/artist/"+id+"?error=failed");
                }
            } else {
                return new ModelAndView("redirect:/artist/"+id);
            }
        }
        return new ModelAndView("redirect:/signin");
    }

    @PostMapping("/artist/artist/new")
    public ModelAndView addArtist(
            @RequestParam Map<String, String> params,
            HttpSession httpSession
    ) {
        if(httpSession.getAttribute("user") != null){

            Artist generatedArtist = handleArtistCreation(params, httpSession);
            if(artistDao.add(generatedArtist) != null){
                return new ModelAndView("redirect:/artist/dashboard?message=artistAdded");
            } else {
                return new ModelAndView("redirect:/artist/artist/new?error=failed");
            }
        }
        return new ModelAndView("redirect:/signin");
    }

    private Artist handleArtistCreation(Map<String, String> params, HttpSession httpSession) {
        if(params.containsKey("name") && params.containsKey("photo-url-1")
        ){
            List<String> photoArray = new ArrayList<>();
            String name = params.get("name");
            String photo1 = params.get("photo-url-1");
            String bio, instagram, twitter;
            boolean ontour;

            photoArray.add(photo1);

            if(params.containsKey("ontour")){
                if(params.get("ontour") == "on"){
                    ontour = true;
                } else {
                    ontour = false;
                }
            } else {
                ontour = false;
            }

            if(params.containsKey("twitter")){
                twitter = params.get("twitter");
            } else {
                twitter = "";
            }

            if(params.containsKey("instagram")){
                instagram = params.get("instagram");
            } else {
                instagram = "";
            }

            if(params.containsKey("bio")){
                bio = params.get("bio");
            } else {
                bio = "";
            }

            for(int i = 2; i <= 10; i++){
                if(params.containsKey("photo-url-" + i)){
                    photoArray.add(params.get("photo-url-" + i));
                }
            }

            Collection<ArtistImage> ai = new ArrayList<>();
            Collection<ArtistSocial> as = new ArrayList<>();
            Artist artist = new Artist();

            ArtistSocial artistSocial = new ArtistSocial();
            artistSocial.setArtistId(artist);
            artistSocial.setTwitter(twitter);
            artistSocial.setInstagram(instagram);

            for(String url : photoArray){
                ArtistImage image = new ArtistImage();
                image.setUrl(url);
                image.setArtistId(artist);
                ai.add(image);
            }

            artist.setName(name);
            artist.setBio(bio);
            artist.setOntour(ontour);
            artist.setArtistImageCollection(ai);
            artist.setArtistSocialCollection(as);
            artist.setUser((User) httpSession.getAttribute("user"));

            return artist;
        } else {
            return null;
        }
    }
}
