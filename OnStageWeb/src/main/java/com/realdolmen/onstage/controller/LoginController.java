package com.realdolmen.onstage.controller;

import com.realdolmen.onstage.beans.User;
import com.realdolmen.onstage.dao.UserDao;
import com.realdolmen.onstage.utils.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpSession;

@Controller
public class LoginController {

    @Autowired
    private UserDao userDao;

    @GetMapping("/signout")
    public ModelAndView signout(
            HttpSession httpSession
    ) {
        if(httpSession.getAttribute("user") != null){
            httpSession.setAttribute("user", null);
        }
        return new ModelAndView("redirect:/signin?message=signedout");
    }

    @GetMapping("/signin")
    public ModelAndView signin(
            @RequestParam(value = "error", defaultValue = "") String error,
            @RequestParam(value = "message", defaultValue = "") String message,
            ModelMap modelMap,
            HttpSession httpSession
    ) {
        if(httpSession.getAttribute("user") == null){
            switch (error){
                case "failed":
                    modelMap.addAttribute("error", "We were unable to register you. Please try again later.");
                    break;
                case "exist":
                    modelMap.addAttribute("error", "This email address and/or username is already in use by someone else.");
                    break;
                case "invalid":
                    modelMap.addAttribute("error", "The email address, username or password are not valid.");
                    break;
                case "password":
                    modelMap.addAttribute("error", "The password you entered is incorrect.");
                    break;
                case "notfound":
                    modelMap.addAttribute("error", "The email address does not exist in our records. Please register first.");
                    break;
            }

            switch(message){
                case "signedout":
                    modelMap.addAttribute("message", "You have been signed out successfully.");
                    break;
            }
            return new ModelAndView("WEB-INF/signin.jsp");
        }
        return new ModelAndView("redirect:/artist/dashboard");
    }

    @PostMapping("/auth/signin")
    public ModelAndView doSignin(
            @RequestParam(name = "signin-email") String email,
            @RequestParam(name = "signin-password") String password,
            HttpSession httpSession
    ){
        if(httpSession.getAttribute("user") == null){
            User user = userDao.findByEmail(email);
            if(user != null){
                if(UserUtil.verifyPassword(password, user.getPassword())){
                    httpSession.setAttribute("user", user);
                    return new ModelAndView("redirect:/artist/dashboard");
                } else {
                    return new ModelAndView("redirect:/signin?error=password");
                }
            } else {
                return new ModelAndView("redirect:/signin?error=notfound");
            }
        }
        return new ModelAndView("redirect:/artist/dashboard");
    }

    @PostMapping("/auth/register")
    public ModelAndView doRegistration(
            @RequestParam(name = "username") String username,
            @RequestParam(name = "email") String email,
            @RequestParam(name = "password") String password,
            HttpSession httpSession
    ) {
        if (httpSession.getAttribute("user") == null){
            User user = new User();
            user.setUsername(username);
            user.setPassword(password);
            user.setEmail(email);

            if(UserUtil.verifyUserFields(user)){
                if(userDao.findByUsername(username) == null && userDao.findByEmail(email) == null){
                    user.setPassword(UserUtil.hashPassword(user.getPassword()));
                    if(userDao.add(user)){
                        httpSession.setAttribute("user", user);
                        return new ModelAndView("redirect:/artist/dashboard");
                    } else {
                        return new ModelAndView("redirect:/signin?error=failed");
                    }
                }  else {
                    return new ModelAndView("redirect:/signin?error=exist");
                }
            }

            return new ModelAndView("redirect:/signin?error=invalid");
        }
        return new ModelAndView("redirect:/artist/dashboard");
    }
}
