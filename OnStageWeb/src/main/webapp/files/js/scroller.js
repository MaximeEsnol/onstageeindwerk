class ScrollWatcher {
    constructor(params) {
        this.element = params.element;
        this.offset = params.offset || 0;
        this.onAppear = params.on_appear || null;
        this.afterAppearAnimation = params.after_appear_animation || null;
        this.onDisappear = params.on_disappear || null;
        this.animationName = params.animation.name || null;
        this.animationDuration = params.animation.duration || 1000;
        this.animationFillMode = params.animation.fill_mode || "none";
        this.animationEasing = params.animation.easing || "ease-in-out";
        this.trigger = params.trigger || "top";
        this.timeout = params.timeout || 0;

        //the last position of the element defaults to -1
        //because 0 might be a valid element position
        this.lastPosition = -1;

        this._init();
    }

    _init = () => {
        document.body.addEventListener("scroll", (event) => this._scrollEvent(event));
    };

    _scrollEvent = (event) => {
        if (this.element.forEach == undefined) {
            if (this.isInViewport(this.element)) {
                if(this.timeout > 0){
                    setTimeout(() => {
                        this._triggerAction();
                    }, this.timeout);
                } else {
                    this._triggerAction();
                }

            }
        } else {
            this._loopThroughElements();
        }
    };

    isInViewport = (elem) => {
        let rect = elem.getBoundingClientRect();

        return this._verifyClientRectValues(rect);
    };

    _triggerAction = (element = null) => {
        if (element == null) {
            element = this.element;
        }

        if (this.onAppear != null) {
            this.onAppear.call(element, element);
        }

        if (this.animationName != null) {
            this._performAnimation(element);
        }
    };

    _performAnimation = (element) => {
        let cssAnimation = this.animationName + " " + this.animationDuration + "ms " + this.animationEasing;
        element.style.animation = cssAnimation;
        element.style.animationFillMode = this.animationFillMode;

        this._afterAnimation(element);
    };

    _afterAnimation = (element) => {
        if(this.afterAppearAnimation != null){
            setTimeout(() => {
                this.afterAppearAnimation.call(element, element);
            }, this.animationDuration);
        }
    };


    /**
     * Checks whether an element's BoundingClientRect is inside the
     * viewport.
     * @param {BoundingClientRect} rect BoundingClientRect from the elemnt. Can be obtained by calling getBoundingClientRect() on an element node.
     * @returns {boolean} true if the element is in the viewport or false if is not.
     * @private
     */
    _verifyClientRectValues = (rect) => {
        if(rect.top >= 0 && rect.left >= 0 && rect.right <= window.innerWidth){
            if(this.trigger == "top"){
                return rect.top <= window.innerHeight
            } else if(this.trigger == "bottom"){
                return rect.bottom <= window.innerHeight;
            } else {
                let parsed = parseInt(this.trigger);
                if(typeof parsed == "number"){
                    return (rect.top + parsed) <= window.innerHeight;
                } else {
                    console.warn("Invalid value was given for 'trigger'.\nAccepted values are: \n - 'top'\n - 'bottom'\n'"+ this.trigger +"' is not valid. Using 'top' as per default.");
                    return rect.top <= window.innerHeight;
                }

            }
        }

    };

    _loopThroughElements = () => {
        this.element.forEach((elem, index) => {
            if (this.isInViewport(elem)) {
                if(this.timeout > 0){
                    setTimeout(() => {
                        this._triggerAction(elem);
                    }, this.timeout);
                } else {
                    this._triggerAction(elem);
                }
            }
        });
    }
}