<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: MEOBL79
  Date: 05/06/2020
  Time: 15:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Artist dashboard - OnStage</title>
    <meta charset="UTF-8"/>
    <link rel="stylesheet" href="../files/css/styles.css"/>
</head>
<body class="app">
    <nav>
        <ul>
            <li>
                <a href="../artist/dashboard">
                    Dashboard home
                </a>
            </li>
            <li>
                <a href="../artist/artist/new">
                    Add artist
                </a>
            </li>
            <li>
                <a href="../artist/tour/add">
                    Add tour
                </a>
            </li>
            <li>
                <a href="../artist/show/add">
                    Add show
                </a>
            </li>
        </ul>
    </nav>
    <main>
        <c:if test="${message != null}">
            <div class="dialog success">
                <p>
                    <c:out value="${message}"/>
                </p>
            </div>
        </c:if>
        <h1>Dashboard home</h1>

        <h2>Artists</h2>
        <div class="artists-list horizontal-list break">
            <div class="item">
                <a href="../artist/artist/new">
                    <div class="add-artist artist-pic">
                        <svg style="width:42px;height:42px" viewBox="0 0 24 24">
                            <path fill="currentColor" d="M12,20C7.59,20 4,16.41 4,12C4,7.59 7.59,4 12,4C16.41,4 20,7.59 20,12C20,16.41 16.41,20 12,20M12,2A10,10 0 0,0 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2M13,7H11V11H7V13H11V17H13V13H17V11H13V7Z" />
                        </svg>
                    </div>
                    <h3>
                        Add an artist
                    </h3>
                </a>
            </div>
            <c:forEach var="artist" items="${artists}">
                <div class="item">
                    <a href="../artist/<c:out value="${artist.id}"/>">
                        <div class="artist-pic" style="background-image: url(<c:out value='${artist.artistImageCollection[0].url}'/>)">

                        </div>
                        <h3>
                            <c:out value="${artist.name}"/>
                        </h3>
                    </a>
                </div>
            </c:forEach>

        </div>

    </main>
</body>
</html>
