<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: MEOBL79
  Date: 07/06/2020
  Time: 12:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Shows from <c:out value="${shows[0].tour.name}"/> - OnStage</title>
    <meta charset="UTF-8"/>
    <link rel="stylesheet" href="../../../files/css/styles.css"/>
</head>
<body class="app">
<nav>
    <ul>
        <li>
            <a href="../../../artist/dashboard">
                Dashboard home
            </a>
        </li>
        <li>
            <a href="../../../artist/artist/new">
                Add artist
            </a>
        </li>
    </ul>
</nav>
<main>
    <c:if test="${message != null}">
        <div class="dialog success">
            <p>
                <c:out value="${message}"/>
            </p>
        </div>
    </c:if>
    <c:if test="${error != null}">
        <div class="dialog error">
            <p>
                <c:out value="${error}"/>
            </p>
        </div>
    </c:if>
    <h1>Shows from <c:out value="${shows[0].tour.name}"/></h1>
    <a href="show/add" class="btn primary">
        Add show
    </a>
    <table>
        <thead>
            <tr>
                <th>
                    Date
                </th>
                <th>
                    Location
                </th>
                <th>
                    Venue
                </th>
                <th>
                    Tickets
                </th>
                <th>
                    Actions
                </th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="show" items="${shows}">
                <tr>
                    <td>
                        <c:out value="${show.date}"/>
                    </td>
                    <td>
                        <c:out value="${show.location}"/>
                    </td>
                    <td>
                        <c:out value="${show.venue}"/>
                    </td>
                    <td>
                        <c:out value="${show.currency}"/>&nbsp;
                        <c:out value="${show.min}"/> - <c:out value="${show.max}"/>
                    </td>
                    <td>
                        <a href="../../show/<c:out value="${show.id}"/>">Edit</a> -
                        <a href="../../show/<c:out value="${show.id}"/>/delete">Delete</a>
                    </td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</main>
</body>
</html>
