import React from 'react';
import {Text} from 'react-native';
import styles from '../constants/Stylesheet';

const Title = props => {
    return(
        <Text style={[styles.title, {...props.style}]}>
            {props.children}
        </Text>
    )
};

export default Title;