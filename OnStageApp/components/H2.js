import React from 'react';
import {Text} from 'react-native';
import styles from '../constants/Stylesheet';

const H2 = props => {
    return(
        <Text style={styles.H2}>
            {props.children}
        </Text>
    )
}

export default H2;