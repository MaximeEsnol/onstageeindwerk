import React from 'react';
import {Text} from 'react-native';
import styles from '../constants/Stylesheet';

const SubText = props => {
    return(
        <Text style={styles.SubText}>
            {props.children}
        </Text>
    )
};

export default SubText;