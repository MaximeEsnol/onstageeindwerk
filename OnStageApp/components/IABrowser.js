import React from 'react';
import {Alert } from 'react-native'
import * as WebBrowser from 'expo-web-browser';
import {ComponentColor} from '../constants/Colors';

const options = {
    toolbarColor: ComponentColor.FOOTER_COLOR,
    enableBarCollapsing: true,
    controlsColor: ComponentColor.FOOTER_CONTENT_COLOR,
    showTitle: true
};

export async function openBrowser(url) {
    try {
        let result = await WebBrowser.openBrowserAsync(url, options);
    } catch (err) {
        Alert.alert("Something went wrong...", "We could not open a browser window.");
        console.log(err);
    }
}