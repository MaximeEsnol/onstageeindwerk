import React, { useEffect, useState, useCallback } from 'react';
import { ScrollView, Text, View, RefreshControl } from 'react-native';
import { getPlannedShows, getDB, findShowById, findTourById } from '../func/Queries';
import MainText from '../components/MainText';
import { Content, ContentTitle } from '../components/Content';
import { DetailedShowCard } from '../components/Cards';
import { Color, ComponentColor } from '../constants/Colors';

const Planned = props => {
    const [planned, setPlanned] = useState(null);
    const [plannedIDsLoaded, setPlannedIDsLoaded] = useState(false);

    const [plannedShows, setPlannedShows] = useState([]);
    const [plannedShowsLoaded, setPlannedShowsLoaded] = useState(false);

    const [refreshing, setRefreshing] = useState(false);
    const db = getDB();
    const showsJSON = require('../assets/shows.json');

    const onRefresh = useCallback(() => {
        setRefreshing(true);
        setPlannedIDsLoaded(false);
        setPlanned(null);
        setPlannedShows([]);
        setPlannedShowsLoaded(false);
        loadPlanned();
        setRefreshing(false);
    }, [refreshing]);


    const loadPlanned = async () => {
        let planned = await getPlannedShows(db);
        setPlanned(planned);
        setPlannedIDsLoaded(true);
    }

    useEffect(() => {
        (async () => {
            if (!plannedIDsLoaded) {
                loadPlanned();
            }
        })();
    });

    if (plannedIDsLoaded && !plannedShowsLoaded) {
        (async () => {
            let current_tour = -1;
            let plannedElems = [];

            for (const plannedID of planned) {
                let show = await findShowById(plannedID.show_id);
                let tourNameElement;
                let current_tour = -1;

                if (show.tour != current_tour) {
                    current_tour = show.tour;
                    let tour = await findTourById(show.tour.id);
                    let tourName = tour.name;
                    tourNameElement = (
                        <ContentTitle>{tourName}</ContentTitle>
                    )
                } else {
                    tourNameElement = null;
                }

                plannedElems.push(
                    <View>
                        {tourNameElement}
                        <DetailedShowCard show={show}
                            onPress={() => {
                                props.navigation.navigate("Show", {
                                    show: show
                                })
                            }}></DetailedShowCard>
                    </View>

                )
            }

            setPlannedShows(plannedElems);
            setPlannedShowsLoaded(true);
        })();
    }

    return (
        <ScrollView refreshControl={<RefreshControl title={"Looking for newly planned shows"} titleColor={ComponentColor.TEXT_COLOR} tintColor={Color.SECONDARY} colors={[Color.SECONDARY, Color.PRIMARY]} refreshing={refreshing} onRefresh={() => onRefresh()} />}>
            <Content>
                {plannedShows}
            </Content>
        </ScrollView>
    )
};

export default Planned;