import React, { useEffect, useState } from 'react';
import { ScrollView, View, Text, Button, TouchableOpacity, ActivityIndicator } from 'react-native';
import H3 from '../components/MainText';
import { HeaderImages } from '../components/Images';
import Title from '../components/Title';
import { Content, ContentTable, ContentTableRow, ContentTableColumn, ContentTitle } from '../components/Content';
import { FontAwesome5 } from '@expo/vector-icons';
import { TableColumnButton, FollowButton } from '../components/Buttons';
import { openBrowser } from '../components/IABrowser';
import H2 from '../components/H2';
import styles from '../constants/Stylesheet';
import { Tag } from '../components/Tags';
import { findToursByArtist, findFirstNearShowFromTour, getDB, getFollowedState, removeFromFollow, addFollow } from '../func/Queries';
import getLocation from '../func/LocationUtil';
import MainText from '../components/MainText';
import { TourCard } from '../components/Cards';
import { ComponentColor, Color } from '../constants/Colors';

const ArtistDetails = props => {
    const { navigation } = props;
    const [followed, setFollowed] = useState(null);
    const [tourData, setTourData] = useState(null);
    const [cards, setCards] = useState([]);
    const [cardsLoaded, setCardsLoaded] = useState(false);
    const [dataLoaded, setDataLoaded] = useState(false);

    const artist = props.route.params.artist;
    console.log(artist);

    useEffect(() => {
        if(!dataLoaded){
            (async() => {
                await loadScreen();
            })();
        }
    });

    const loadScreen = async () => {
        let tours = await findToursByArtist(artist.id);
        setTourData(tours);
        setDataLoaded(true);
    }

    if(dataLoaded){
        if(!cardsLoaded){
            let artistTours = [];

            for (const tour of tourData) {
                artistTours.push(
                    <TourCard tour={tour}
                        onTourPress={() => props.navigation.navigate(
                            "Tour",
                            {
                                tour: tour
                            }
                        )}></TourCard>
                );
            }
    
            setCards(artistTours);
            setCardsLoaded(true);
        }
    }

    let touring_tag;

    if (artist.ontour) {
        touring_tag = (
            <Tag importance={"ok"}>
                Touring
            </Tag>
        );
    }

    return (
        <ScrollView>
            <HeaderImages images={artist.artistImageCollection} />
            <Content>
                <View style={styles.artistTitle_Follow}>
                    <Title>{artist.name}</Title>
                    {/* <FollowButton isFollowed={followed}
                        unfollow={() => removeFromFollow(db, artist.id).then(setFollowed(null))}
                        follow={() => addFollow(db, artist.id).then(setFollowed(null))}></FollowButton> */}
                </View>

                <ContentTable>
                    <ContentTableRow>
                        <ContentTableColumn>
                            <TableColumnButton onPress={() => openBrowser("https://twitter.com/" + artist.artistSocialCollection[0].twitter)}>
                                <H3>{artist.artistSocialCollection[0].twitter}</H3>
                                <FontAwesome5 name={"twitter"} size={24} color={"white"}></FontAwesome5>
                            </TableColumnButton>

                        </ContentTableColumn>
                        <ContentTableColumn>
                            <TableColumnButton onPress={() => openBrowser("https://instagram.com/" + artist.artistSocialCollection[0].instagram)}>
                                <H3>{artist.artistSocialCollection[0].instagram}</H3>
                                <FontAwesome5 name={"instagram"} size={24} color={"white"}></FontAwesome5>
                            </TableColumnButton>
                        </ContentTableColumn>
                    </ContentTableRow>
                </ContentTable>

                <View style={styles.flexRow}>
                    <ContentTitle>
                        Tours
                    </ContentTitle>
                    {touring_tag}
                </View>

                {cards}

            </Content>
        </ScrollView>
    )
};


export default ArtistDetails;