import React, { useState, useEffect, useCallback } from 'react';
import { ScrollView, View, ActivityIndicator, RefreshControl, Text } from 'react-native';
import styles from "../constants/Stylesheet";
import Title from '../components/Title';
import MainText from '../components/MainText';
import { Content, ContentTitle } from '../components/Content';
import { findNearbyShows, getFollowedArtists, getDB, findArtistById, findPickedTours, findTourById } from '../func/Queries';
import { Color, ComponentColor } from '../constants/Colors';

import getLocation from '../func/LocationUtil';
import { ShowCard, ArtistCardBig, MoreCard, LargeCard } from '../components/Cards';

console.disableYellowBox = true;


const Home = props => {
    const { navigation } = props;


    const [location, setLocation] = useState(null);
    const [longitude, setLongitude] = useState(0);
    const [latitude, setLatitude] = useState(0);

    const [nearbyShows, setNearbyShows] = useState(null);
    const [cardNearbyShows, setCardNearbyShows] = useState([]);
    const [nearbyShowsLoaded, setNearbyShowsLoaded] = useState(false);

    const [refreshing, setRefreshing] = useState(false);

    const db = getDB();

    const onRefresh = useCallback(() => {
        setRefreshing(true);
        setNearbyShowsLoaded(false);
        setLocation(null);
        setNearbyShows(null);
        setCardNearbyShows([]);
        loadHomePage();
        setRefreshing(false);
    }, [refreshing]);

    const loadHomePage = async () => {
        await loadNearby();
    }

    const loadNearby = async () => {
        if (!location) {
            let location = await getLocation();
            
            setLocation(location);
        }

        setLongitude(location.coords.longitude);
            setLatitude(location.coords.latitude);

        if (nearbyShows == null) {
            let nearbyShows = await findNearbyShows(latitude, longitude);
            setNearbyShows(nearbyShows);
        }

    }

    if (nearbyShows != null) {
        (async () => {
            if (!nearbyShowsLoaded) {
                let nearbyShowsCards = [];
                for (const show of nearbyShows) {
                    let tour = await findTourById(show.tour.id);
                    let artist = await findArtistById(tour.artist.id);

                    nearbyShowsCards.push(
                        <ShowCard key={"show" + show.id + "-" + tour.id}
                            show={show}
                            tour={tour}
                            artist={artist}
                            onPress={() => props.navigation.navigate("Show", {
                                show: show,
                                artist: artist,
                                tour: tour
                            })}></ShowCard>
                    )
                }

                setCardNearbyShows(nearbyShowsCards);
                setNearbyShowsLoaded(true);
            }
        })();

    }

    useEffect(() => {
        (async () => {
            loadHomePage();
        })();
    });

    return (
        <ScrollView refreshControl={<RefreshControl
            title={"Looking for new shows"}
            titleColor={ComponentColor.TEXT_COLOR}
            tintColor={Color.SECONDARY}
            colors={[Color.SECONDARY, Color.PRIMARY]}
            refreshing={refreshing}
            onRefresh={() => onRefresh()} />}
            contentContainerStyle={styles.body}>
            <Content>
                <ContentTitle>Shows near you</ContentTitle>
            </Content>

            <View style={styles.cardContainer}>
                {cardNearbyShows}
            </View>
        </ScrollView>
    )
};

export default Home;