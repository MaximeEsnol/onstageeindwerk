import React, { useState, useEffect } from 'react';
import { ScrollView, View, TouchableOpacity, Alert } from 'react-native';
import { findTourById, findArtistById, addShowToPlanned, removeShowFromPlanned, getDB, isShowPlanned } from '../func/Queries';
import { Content, ContentTitle, ContentTable, ContentTableRow, ContentTableColumn } from '../components/Content';
import { ComponentColor } from '../constants/Colors';
import { HeaderImage } from '../components/Images';
import styles from '../constants/Stylesheet';
import SubText from '../components/SubText';
import { getTextDate, getTimeFromDateString, getTimeFromDate } from '../func/DateUtil';
import { FontAwesome5 } from '@expo/vector-icons';
import { IconButton, TextButton } from '../components/Buttons';
import Title from '../components/Title';
import { Tag } from '../components/Tags';
import H3 from '../components/H3';
import { openBrowser } from '../components/IABrowser';
import MainText from '../components/MainText';


const getTagFromAvailability = (availability) => {
    let tag;

    switch (availability.toLowerCase()) {
        case "good":
            tag = (
                <Tag importance={"ok"}>
                    On sale
                </Tag>
            );
            break;
        case "decent":
            tag = (
                <Tag importance={"mild"}>
                    Few left
                </Tag>
            );
            break;
        case "last":
            tag = (
                <Tag importance={"risk"}>
                    Last tickets
                </Tag>
            );
            break;
        case "soldout":
            tag = (
                <Tag importance={"warning"}>
                    Sold out
                </Tag>
            );
            break;
        case "cancelled":
            tag = (
                <Tag importance="warning">
                    Cancelled
                </Tag>
            );
            break;
    }

    return tag;
};

const Show = props => {
    let show = props.route.params.show;
    let textDate = getTextDate(show.date);

    const [icon, setIcon] = useState(null);
    const [content, setContent] = useState(null);
    const [tour, setTour] = useState(null);
    const [dataLoaded, setDataLoaded] = useState(false);
    const [artist, setArtist] = useState(null);
    const db = getDB();

    useEffect(() => {
        (async () => {
            if(!dataLoaded){
                await loadShowScreen();
            }
        })();
    })

    const loadShowScreen = async () => {
        let tour = await findTourById(show.tour.id);
        props.navigation.setOptions({
            title: tour.name + " - " + show.location
        });
        let artist = await findArtistById(tour.artist.id);
        setArtist(artist);
        setTour(tour);
        setDataLoaded(true);
    }


    const toggleAddToPlanned = async () => {
        if (show.availability != "cancelled") {
            if (icon === "calendar-plus") {
                setIcon("calendar-minus");
                await addShowToPlanned(db, show.id);
            } else {
                setIcon("calendar-plus");
                await removeShowFromPlanned(db, show.id);
            }
        } else {
            await removeShowFromPlanned(db, show.id);
            Alert.alert("This show has been cancelled.",
                "We're so sorry, but it seems this show has been cancelled. You can't plan cancelled shows.");
        }

    };

    useEffect(() => {
        (async () => {
            if (icon == null) {
                let show_already_saved = await isShowPlanned(db, show.id);
                if (show_already_saved) {
                    setIcon("calendar-minus");
                } else {
                    setIcon("calendar-plus");
                }
            }

        })();
    })

    let currency;
    let minPrice = "" + show.min;
    let maxPrice = "" + show.max;

    switch (show.currency) {
        case "EUR":
            currency = "€";
            minPrice = minPrice.replace(".", ",");
            maxPrice = maxPrice.replace(".", ",");
            break;
        case "USD":
            currency = "$";
            break;
        case "GBP":
            currency = "£";
            break;
        default:
            currency = "€";
            minPrice = minPrice.replace(".", ",");
            maxPrice = maxPrice.replace(".", ",");
            break;
    }

    let buyButton;

    if (show.ticketing) {
        buyButton = (
            <View style={styles.BuyTickets_Container}>
                <TextButton style={{ width: "50%" }} onPress={() => openBrowser(show.ticketing)}>
                    Buy tickets
            </TextButton>
            </View>
        );
    }

    if (artist != null && tour != null && content == null) {
        setContent(
            <View>
                <HeaderImage style={styles.alignBottom} image={{ uri: artist.artistImageCollection[0].url }}>
                </HeaderImage>

                <View style={styles.Content_RightButton}>
                    <Content style={styles.Content_LeftSideLong}>
                        <TouchableOpacity onPress={() => {
                            props.navigation.navigate("Tour", {
                                tour: tour
                            });
                        }}>
                            <Title>{tour.name}</Title>
                            <SubText>{show.location}, {textDate}</SubText>
                        </TouchableOpacity>
                    </Content>
                    <IconButton style={styles.Content_RightSideShort} onPress={() => toggleAddToPlanned()}>
                        <FontAwesome5 name={icon} size={24} color={ComponentColor.TEXT_COLOR} />
                    </IconButton>
                </View>

                <View style={styles.Show_Prices}>
                    <Content>
                        <View style={styles.flexRow}>
                            <ContentTitle>
                                Tickets
                        </ContentTitle>
                            {getTagFromAvailability(show.availability)}
                        </View>
                        <ContentTable>
                            <ContentTableRow>
                                <ContentTableColumn key="minPrice">
                                    <H3>
                                        {currency} {minPrice}
                                    </H3>
                                    <SubText>Min. price</SubText>
                                </ContentTableColumn>
                                <ContentTableColumn key="maxPrice">
                                    <H3>
                                        {currency} {maxPrice}
                                    </H3>
                                    <SubText>Max. price</SubText>
                                </ContentTableColumn>
                            </ContentTableRow>
                        </ContentTable>
                    </Content>

                    {buyButton}

                    <Content>
                        <ContentTitle>
                            Show info
                    </ContentTitle>

                        <ContentTable>
                            <ContentTableRow>
                                <ContentTableColumn>
                                    <H3>{show.venue}</H3>
                                    <SubText>Venue</SubText>
                                </ContentTableColumn>
                                <ContentTableColumn>
                                    <H3>{show.location}</H3>
                                    <SubText>City</SubText>
                                </ContentTableColumn>
                            </ContentTableRow>

                            <ContentTableRow>
                                <ContentTableColumn>
                                    <H3>{getTimeFromDate(show.time)}</H3>
                                    <SubText>Doors open</SubText>
                                </ContentTableColumn>
                            </ContentTableRow>
                        </ContentTable>

                        <MainText>
                            {tour.description}
                        </MainText>
                    </Content>
                </View>
            </View>
        )
    }

    return (
        <ScrollView bouncesZoom={true}>
            {content}
        </ScrollView>
    )
};

export default Show;